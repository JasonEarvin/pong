using System;
using UnityEngine;
using UnityEngine.UI;

namespace Pong
{
    public class GameOverPopupController : MonoBehaviour
    {
        [SerializeField] private Canvas canvas;
        [SerializeField] private Button playAgainButton;
        [SerializeField] private Button quitButton;
        [SerializeField] private Text finalScoreText;

        [SerializeField] private AudioSource audioSource;

        public event Action PlayAgainPressed;
        public event Action QuitPressed;

        private void Start()
        {
            playAgainButton.onClick.AddListener(OnPlayAgainPressed);
            quitButton.onClick.AddListener(OnQuitPressed);
        }

        private void OnDestroy()
        {
            playAgainButton.onClick.RemoveListener(OnPlayAgainPressed);
            quitButton.onClick.RemoveListener(OnQuitPressed);
        }

        private void OnPlayAgainPressed()
        {
            Hide();
            PlayAgainPressed?.Invoke();
        }
        private void OnQuitPressed()
        {
            Hide();
            QuitPressed?.Invoke();
        }

        public void Show(int num)
        {
            if(num == 0)
            {
                finalScoreText.text = "A Wins";
            }

            else
            {
                finalScoreText.text = "B Wins";
            }

            audioSource.Play();

            canvas.enabled = true;
        }

        public void Hide()
        {
            canvas.enabled = false;
        }
    }
}