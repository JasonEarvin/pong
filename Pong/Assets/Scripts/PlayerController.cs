using UnityEngine;

namespace Pong
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private KeyCode upInput;
        [SerializeField] private KeyCode downInput;
        [SerializeField] private float upperLimit;
        [SerializeField] private float lowerLimit;

        private bool isActive = true;
        private Vector3 movespeed = new Vector3(0f, 5f, 0f);

        private void Update()
        {
            if (!isActive) return;
            if (Input.GetKey(upInput) && transform.position.y < upperLimit)
            {
                transform.Translate(movespeed * Time.deltaTime);
            }

            else if (Input.GetKey(downInput) && transform.position.y > lowerLimit)
            {
                transform.Translate(-movespeed * Time.deltaTime);
            }
        }

        public void Activate()
        {
            isActive = true;
        }

        public void Deactivate()
        {
            isActive = false;
        }
    }
}


