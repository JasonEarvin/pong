using System;
using System.Collections;
using UnityEngine;

namespace Pong
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private BallController ball;
        [SerializeField] private PlayerController playerA;
        [SerializeField] private PlayerController playerB;
        [SerializeField] private GoalController goalA;
        [SerializeField] private GoalController goalB;
        [SerializeField] private PausePopupController pausePopup;
        [SerializeField] private GameOverPopupController gameOverPopup;

        private const int FinalScore = 3;

        private int scoreA = 0;
        private int scoreB = 0;
        private bool isPaused;
        private int lastScored = 1;
        private GameState currentState = GameState.None;

        public event Action<int, int> ScoreChanged;

        public void Awake()
        {
            goalA.GoalScored += OnGoalScored;
            goalB.GoalScored += OnGoalScored;

            pausePopup.ContinuePressed += OnContinuePressed;
            pausePopup.ResetPressed += OnContinuePressed;
            pausePopup.QuitPressed += OnContinuePressed;

            gameOverPopup.PlayAgainPressed += OnResetPressed;
            gameOverPopup.QuitPressed += OnQuitPressed;

            currentState = GameState.Waiting;
        }

        private void OnDestroy()
        {
            goalA.GoalScored -= OnGoalScored;
            goalB.GoalScored -= OnGoalScored;

            pausePopup.ContinuePressed -= OnContinuePressed;
            pausePopup.ResetPressed -= OnContinuePressed;
            pausePopup.QuitPressed -= OnContinuePressed;

            gameOverPopup.PlayAgainPressed -= OnResetPressed;
            gameOverPopup.QuitPressed -= OnQuitPressed;
        }

        private void StartGame()
        {
            EjectBall();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Pause();
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (currentState == GameState.Waiting)
                {
                    currentState = GameState.Playing;
                    StartGame();
                }
            }
        }

        #region Button Press
        private void OnContinuePressed()
        {
            Pause();
        }

        private void OnResetPressed()
        {
            scoreA = scoreB = 0;
            ball.Reset();
            ball.Deactivate();
            currentState = GameState.Waiting;
        }

        private void OnQuitPressed()
        {
            Application.Quit();
        }
        #endregion

        private void EjectBall()
        {
            ball.Reset();
            ball.RandomizeDirection(lastScored == 0 ? 1 : 0);
            ball.Activate();
        }

        private void OnGoalScored(int num = 0)
        {
            if (num == 0)
            {
                scoreB++;
            }

            else if (num == 1)
            {
                scoreA++;
            }

            ScoreChanged?.Invoke(scoreA, scoreB);

            if (GameOverCheck()) return;
            lastScored = num;

            ball.Reset();
            Invoke(nameof(EjectBall), 1f);
        }

        private void Pause()
        {
            isPaused = !isPaused;
            if (isPaused)
            {
                ball.Deactivate();
                playerA.Deactivate();
                playerB.Deactivate();
                pausePopup.Show();
            }

            else
            {
                ball.Activate();
                playerA.Activate();
                playerB.Activate();
                pausePopup.Hide();
            }
        }

        private bool GameOverCheck()
        {
            if (scoreA == FinalScore || scoreB == FinalScore)
            {
                gameOverPopup.Show(scoreA == FinalScore ? 0 : 1);
                return true;
            }

            return false;
        }
    }
}


