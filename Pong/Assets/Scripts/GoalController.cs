using System;
using UnityEngine;

namespace Pong
{
    public class GoalController : MonoBehaviour
    {
        [SerializeField] private int id;
        [SerializeField] private GameObject ball;
        [SerializeField] private AudioSource audioSource;

        public event Action<int> GoalScored;

        private void OnTriggerEnter2D(Collider2D other)
        {
            Debug.Log("hit " + other.name);
            if (other.gameObject.GetInstanceID() == ball.GetInstanceID())
            {
                GoalScored?.Invoke(id);
                audioSource.Play();
            }
        }
    }
}


