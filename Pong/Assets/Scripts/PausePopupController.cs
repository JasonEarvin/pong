using System;
using UnityEngine;
using UnityEngine.UI;

namespace Pong
{
    public class PausePopupController : MonoBehaviour
    {
        [SerializeField] private Canvas canvas;
        [SerializeField] private Button continueButton;
        [SerializeField] private Button resetButton;
        [SerializeField] private Button quitButton;

        public event Action ContinuePressed;
        public event Action ResetPressed;
        public event Action QuitPressed;

        [SerializeField] private AudioSource audioSource;

        private void Start()
        {
            continueButton.onClick.AddListener(OnContinuePressed);
            resetButton.onClick.AddListener(OnResetPressed);
            quitButton.onClick.AddListener(OnQuitPressed);
        }

        private void OnDestroy()
        {
            continueButton.onClick.RemoveListener(OnContinuePressed);
            resetButton.onClick.RemoveListener(OnResetPressed);
            quitButton.onClick.RemoveListener(OnQuitPressed);
        }

        private void OnContinuePressed()
        {
            Hide();
            ContinuePressed?.Invoke();
        }
        private void OnResetPressed()
        {
            Hide();
            ResetPressed?.Invoke();
        }
        private void OnQuitPressed()
        {
            Hide();
            QuitPressed?.Invoke();
        }

        public void Show()
        {
            canvas.enabled = true;
            audioSource.Play();
        }

        public void Hide()
        {
            canvas.enabled = false;
        }
    }
}


