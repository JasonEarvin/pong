using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuCrontroller : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] GameObject startMenu;

    public void StartPressed(int sceneID)
    {
        SceneManager.LoadScene(sceneID);
    }

    public void QuitPressed()
    {
        
    }
}
