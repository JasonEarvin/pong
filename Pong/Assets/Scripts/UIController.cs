using UnityEngine;
using UnityEngine.UI;

namespace Pong
{
    public class UIController : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private Text scoreAText;
        [SerializeField] private Text scoreBText;

        void Start()
        {
            gameManager.ScoreChanged += OnScoreChanged;
        }

        private void OnScoreChanged(int score1, int score2)
        {
            scoreAText.text = score1.ToString();
            scoreBText.text = score2.ToString();
        }
    }
}


