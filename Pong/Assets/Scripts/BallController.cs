using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Pong
{
    public class BallController : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D rb;
        [SerializeField] private Vector2 direction;
        private float speed = 10f;
        private bool isActive = false;

        [SerializeField] private ContactFilter2D _contactFilter2D;
		private List<RaycastHit2D> _castCollisions = new List<RaycastHit2D>();

        private void FixedUpdate()
        {
            if (!isActive) return;

            rb.MovePosition(rb.position + (direction * Time.fixedDeltaTime));
        }

        public void Reset()
        {
            rb.position = Vector2.zero;
            direction = Vector2.zero;
        }

        public void Activate()
        {
            isActive = true;
        }

        public void Deactivate()
        {
            isActive = false;
        }

        public void RandomizeDirection(int side)
        {
            int dir = side == 0 ? -1 : 1;
            direction = new Vector2(dir, Random.Range(0, 3f)).normalized * speed;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.name.Contains("Wall"))
            {
                direction.y *= -1f;
            }

            else if (other.gameObject.name.Contains("Player"))
            {
                direction.x *= -1f;

            }
        }

    }
}


